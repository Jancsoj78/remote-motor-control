/*
  Arduino FS-I6X Demo
  fsi6x-arduino-uno.ino
  Read output ports from FS-IA6B receiver module
  Display values on Serial Monitor
  
  Channel functions by Ricardo Paiva - https://gist.github.com/werneckpaiva/
  
  DroneBot Workshop 2021
  https://dronebotworkshop.com
*/
 
// Define Input Connections
#define CH1 3
#define CH2 5
#define CH3 6
#define CH4 9
#define CH5 10
#define CH6 11
 
// Integers to represent values from sticks and pots
int ch1Value;
int ch2Value;
int ch3Value;
int ch4Value; 
int ch5Value;

int forgas;
int elore;
float floatforgas;
float floatelore;

int RightMotor = 9;  // pin maping according to schematics
int LeftMotor = 10; 
int R_dir = 8;
int L_dir = 12;
 
// Boolean to represent switch value
bool ch6Value;
 
// Read the number of a specified channel and convert to the range provided.
// If the channel is off, return the default value
int readChannel(int channelInput, int minLimit, int maxLimit, int defaultValue){
  int ch = pulseIn(channelInput, HIGH, 30000);
  if (ch < 100) return defaultValue;
  return map(ch, 1000, 2000, minLimit, maxLimit);
}
 
// Read the switch channel and return a boolean value
bool readSwitch(byte channelInput, bool defaultValue){
  int intDefaultValue = (defaultValue)? 100: 0;
  int ch = readChannel(channelInput, 0, 100, intDefaultValue);
  return (ch > 50);
}
 
void setup(){
  // Set up serial monitor
  Serial.begin(115200);
  
  // Set all pins as inputs
//  pinMode(CH1, INPUT);
  pinMode(CH2, INPUT);
  pinMode(CH3, INPUT);
//  pinMode(CH4, INPUT);
//  pinMode(CH5, INPUT);
//  pinMode(CH6, INPUT);

  // reducing the PWM freq

TCCR1B = TCCR1B & 0B11111000 | 0B00000100;

//pinMode(RightMotor,OUTPUT);
//pinMode(LeftMotor,OUTPUT);
pinMode(R_dir,OUTPUT);
pinMode(L_dir,OUTPUT);

}
  
void loop() {

  forgas = readChannel(CH3, -255, 255, 0);
  elore = readChannel(CH2, -255, 255, 0);
 
  floatforgas =forgas*1.4;
  forgas = floatforgas;
  floatelore = elore*1.25;
  elore = floatelore;


  Serial.print(" ch3-forgas: ");
  Serial.print(forgas);
  Serial.print(" ch2-elore: ");
  Serial.print(elore);
  Serial.print(" float-elore: ");
  Serial.println(floatelore);  

   if (forgas <= 90) 
   {
     if (forgas <= -90) 
       {Motorforogjobb(abs(forgas+20));} 
     else 
     { 
       if (elore >= 50) 
         {Motorelore(elore);} 
       else 
         if (elore <= -50) 
           {Motorhatra(abs(elore));}
         else 
           StopMotor();}
     }
   else 
   {Motorforogbal(forgas-20);};   
  
  delay(50);
};
void Motorforogbal(unsigned int Pwm)
{
  analogWrite(LeftMotor,Pwm);
  digitalWrite(L_dir, LOW);
  analogWrite(RightMotor,Pwm);
  digitalWrite(R_dir, HIGH);  
}
void Motorforogjobb(unsigned int Pwm)
{
  analogWrite(LeftMotor,Pwm);
  digitalWrite(L_dir, HIGH);
  analogWrite(RightMotor,Pwm);
  digitalWrite(R_dir, LOW);  
}

void Motorelore(unsigned int Pwm)
{
  analogWrite(LeftMotor,Pwm);
  digitalWrite(L_dir, LOW);
  analogWrite(RightMotor,Pwm);
  digitalWrite(R_dir, LOW);  
}
void Motorhatra(unsigned int Pwm)
{
  analogWrite(LeftMotor,Pwm);
  digitalWrite(L_dir, HIGH);
  analogWrite(RightMotor,Pwm);
  digitalWrite(R_dir, HIGH);  
}
void StopMotor()
{
  digitalWrite(LeftMotor,LOW);
  digitalWrite(RightMotor,LOW);  
}
