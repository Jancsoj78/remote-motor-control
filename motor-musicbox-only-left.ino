//int RightMotor = 9; // pin maping according to schematics
int LeftMotor = 10;
//int R_dir = 8;
int L_dir = 12;
int speed = 255;
int time = 300;

void setup(){
 // reducing the pwm freq
TCCR1B = TCCR1B & 0B11111000 | 0B00000100;

 // Set up serial monitor
  Serial.begin(115200);
  
//pinMode(RightMotor,OUTPUT);
pinMode(LeftMotor,OUTPUT);
//pinMode(R_dir,OUTPUT);
pinMode(L_dir,OUTPUT);
}

void loop(){

 // Print to Serial Monitor
  
  speed= 255;
  time = 300;
  
  Serial.print("speed: ");
  Serial.print(speed);
  Serial.print(" | time: ");
  Serial.print(time);
  Serial.print("\n");
   
 Forward(speed);
 delay(time);

 // Print to Serial Monitor
  speed= 255;
  time = 300;
  
  Serial.print("speed: ");
  Serial.print(speed);
  Serial.print(" | time: ");
  Serial.print(time);
  Serial.print("\n");
 
 Backward(speed);
 delay(time);

 // Print to Serial Monitor

  speed= 128;
  time = 300;
  
  Serial.print("speed: ");
  Serial.print(speed);
  Serial.print(" | time: ");
  Serial.print(time);
  Serial.print("\n");
  
 Forward(speed);
 delay(time);

 // Print to Serial Monitor

  speed= 128;
  time = 300;
    
  Serial.print("speed: ");
  Serial.print(speed);
  Serial.print(" | time: ");
  Serial.print(time);
  Serial.print("\n");

 
 Backward(speed);
 delay(time);

 // Print to Serial Monitor
  speed= 95;
  time = 300;
  
  Serial.print("speed: ");
  Serial.print(speed);
  Serial.print(" | time: ");
  Serial.print(time);
  Serial.print("\n");

 Forward(speed);
 delay(time);

 // Print to Serial Monitor
  speed= 95;
  time = 300;
  
  Serial.print("speed: ");
  Serial.print(speed);
  Serial.print(" | time: ");
  Serial.print(time);
  Serial.print("\n");

 Backward(speed);
 delay(time);

 speed= 50;
  time = 3000;
  
  Serial.print("speed: ");
  Serial.print(speed);
  Serial.print(" | time: ");
  Serial.print(time);
  Serial.print("\n");

 Forward(speed);
 delay(time);

 // Print to Serial Monitor
  speed= 50;
  time = 3000;
  
  Serial.print("speed: ");
  Serial.print(speed);
  Serial.print(" | time: ");
  Serial.print(time);
  Serial.print("\n");

 Backward(speed);
 delay(time);


 // Print to Serial Monitor
  speed= 0;
  time = 1500;
  
  Serial.print("loop - speed: ");
  Serial.print(speed);
  Serial.print(" | time: ");
  Serial.print(time);
  Serial.print("\n");
 
 Backward(speed);
 delay(time);
}
void Forward(unsigned int Pwm)
{
// digitalWrite(R_dir,LOW); 
 digitalWrite(L_dir,LOW);
// analogWrite(RightMotor,Pwm);
 analogWrite(LeftMotor,Pwm);
}
void Backward(unsigned int Pwm)
{
// digitalWrite(R_dir,HIGH);
 digitalWrite(L_dir,HIGH);
// analogWrite(RightMotor,Pwm);
 analogWrite(LeftMotor,Pwm);
}
